const router = require('express').Router()
const userController = require('../controller/userController')
const guiaController = require('../controller/guiaContoller')
const reservaController = require('../controller/reservaController')

//rotas para o user controller(CRUD) 
router.post('/postUser/', userController.postUser)
router.post('/user/', userController.postUser)
router.put('/editaruser/:id_usuario', userController.editarUser)
router.delete('/user/', userController.deleteUser)
router.get('/user/', userController.getUserDetails)
router.post('/login/', userController.postLogin)

//rotas para a Guia Controller(CRUD) 
router.post('/guia/', guiaController.postGuia)
router.put('/editarguia/', guiaController.updateGuia)
router.delete('/guia/', guiaController.deleteGuia)
router.get('/guia/', guiaController.getGuia)

//rotas para Reservas
router.post("/reserva", reservaController.createReserva);
router.get("/listarreservas", reservaController.getAllReservas);
router.put("/editarreserva/:id_reserva", reservaController.updateReserva);
router.delete("/reserva/:id_reserva", reservaController.deleteReserva);


module.exports = router;