const connect = require("../db/connect");

module.exports = class userController {
  // Criar um novo usuário
  static async postUser(req, res) {
    try {
      const { nome, senha, email, telefone, confirmarSenha } = req.body;
    // Inicializa a variável de mensagem de erro
      let msgerro = "";

      // Validações  
      // Verifica se o email contém '@'
      if (!email.includes("@")) {
        msgerro = "O email deve conter '@'.";
      // Verifica se a senha tem pelo menos 4 caracteres
      } else if (senha.length < 4) {  
        msgerro = "A senha deve ter no mínimo 4 caracteres.";
      // Verifica se o nome começa com letra maiúscula
      } else if (!/^[A-ZÀ-Ú]/.test(nome)) {
        msgerro = "O nome do usuário deve começar com letra maiúscula.";
      } else if (
        nome === "" ||
        senha === "" ||
        email === "" ||
        telefone === ""
      ) {
      // Verifica se há campos vazios
        msgerro = "Campos inválidos.";
      } else if (senha !== confirmarSenha) {
      // Verifica se as senhas coincidem
        msgerro = "As senhas não coincidem.";
      } else {
      // Se todas as validações derem certo, define a mensagem de sucesso!
        msgerro = "Sucesso!";
      }

       // Registra o email no console
      console.log("Email", email);

      // Se houver erro nas validações, retorna uma resposta de erro com a mensagem correspondente
      if (msgerro !== "Sucesso!") {
        return res.status(400).json({ message: msgerro });
      } else {  // Se não houver erros nas validações, executa a inserção no banco de dados
        const query = `INSERT INTO usuario (nome, senha, email, telefone) VALUES ('${nome}', '${senha}', '${email}', '${telefone}')`;
        // Executa a query de inserção no banco de dados
        connect.query(query, function (err) {
          // Se houver erro na inserção, retorna uma resposta de erro
          if (err) {
        // Registra o erro no console
            console.log(err);
            res.status(500).json({ error: "Erro ao cadastrar usuário" });
            return;
          }
          // Registra no console que o usuário foi inserido no banco com sucesso
          console.log("Inserido no Banco!!!");
          res.status(201).json({ message: "Usuário cadastrado com sucesso." });
        });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  // Editar o usuário já logado
  static async editarUser(req, res) {
    const { id_usuario } = req.params;
    const { nome, senha, email, telefone, confirmarSenha } = req.body;
  // Inicializa a variável de mensagem de erro
    let msgerro = "";

    // Validações
    // Verifica se o email contém '@'
    if (!email.includes("@")) {
      msgerro = "O email deve conter '@'.";
    // Verifica se a senha tem pelo menos 4 caracteres
    } else if (senha.length < 4) {
      msgerro = "A senha deve ter no mínimo 4 caracteres.";
    // Verifica se o nome começa com letra maiúscula
    } else if (!/^[A-ZÀ-Ú]/.test(nome)) {
      msgerro = "O nome do usuário deve começar com letra maiúscula.";
       // Verifica se há campos vazios
    } else if (nome === "" || senha === "" || email === "" || telefone === "") {
      msgerro = "Campos inválidos.";
    } else if (senha !== confirmarSenha) {
      // Verifica se as senhas coincidem
      msgerro = "As senhas não coincidem.";
    }

    if (msgerro) {
      return res.status(400).json({ error: msgerro });
      // Se não houver mensagem de erro, executa a atualização no banco de dados
    } else {
      //query de atualização
      const query = `UPDATE usuario SET nome = ?, senha = ?, email = ?, telefone = ? WHERE id_usuario = ?`;
      // Define os valores a serem inseridos na query
      const values = [nome, senha, email, telefone, id_usuario];

      // Executa a query de atualização no banco de dados
      connect.query(query, values, function (err) {
        if (err) {
      // Registra o erro no console
          console.log(err);
          return res
            .status(500)
            .json({ error: "Erro ao atualizar o usuário no banco!!!" });
        }
        console.log("Atualizado no Banco!!!");
        return res
          .status(200)
          .json({ message: "Usuário atualizado com sucesso." });
      });
    }
  }

  // Deletar o usuário logado
  static async deleteUser(req, res) {
  // Extrai o id do usuário a ser deletado do corpo da requisição
    const { id } = req.body;

    //Query de deletar
    const query = `DELETE FROM usuario WHERE id_usuario = ${id}`;
    // Executa a query de deletar no banco de dados
    connect.query(query, function (err) {
    // Se houver erro, retorna uma resposta de erro
      if (err) {
        console.log(err);
        return res
          .status(500)
          .json({ error: "Erro ao deletar o usuário no banco!!!" });
      }
      console.log("Deletado do Banco!!!");
      return res.status(200).json({ message: "Usuário deletado com sucesso." });
    });
  }

  // Buscar os detalhes do usuário 
  static async getUserDetails(req, res) {
    const { id } = req.body;

    // query de seleção dos detalhes do usuário
    const query = `SELECT * FROM usuario WHERE id_usuario = ${id}`;
    connect.query(query, function (err, results) {
      if (err) {
        console.log(err);
        return res
          .status(500)
          .json({ error: "Erro ao buscar detalhes do usuário" });
      }
       // Se nenhum usuário for encontrado com o id especificado, retorna uma resposta de erro
      if (results.length === 0) {
        return res.status(404).json({ error: "Usuário não encontrado" });
      }
    // Retorna uma resposta de sucesso ao cliente com os detalhes do usuário encontrado
      return res.status(200).json({ usuario : results[0] });
    });
  }

  //Fazer login
  static async postLogin(req, res) {
    // Extrai o email e a senha 
    const { email, senha } = req.body;
  
  // Verifica se o email e a senha foram fornecidos
    if (!email || !senha) {
      return res.status(400).json({ error: "Email e senha são obrigatórios" });
    }
   //query de seleção do usuário com base no email e senha fornecidos
    const query = `SELECT * FROM usuario WHERE email = '${email}' AND senha = '${senha}'`;

    try {
      connect.query(query, function (err, results) {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }
// Se nenhum usuário for encontrado com as credenciais fornecidas, retorna uma resposta de erro
        if (results.length === 0) {
      // Retorna uma resposta de erro indicando credenciais inválidas
          return res.status(401).json({ error: "Credenciais inválidas" });
        }

        return res
          .status(200)
          .json({ message: "Login realizado com sucesso", user: results[0] });
      });
    // Captura erros inesperados
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
    // Retorna uma resposta de erro ao cliente indicando um erro interno do servidor
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }
};