// Importa o módulo de conexão com o banco de dados
const connect = require("../db/connect");

//Exportando a reservaController 
module.exports = class reservaController {

  //Função para criação de reserva
  //POST
  static async createReserva(req, res) {
     // Extrai os dados necessários do corpo da requisição
    const { id_guia, dataInit, dataEnd, periodosReservados, id_usuario } = req.body;
    
    console.log("valores: ", id_guia, dataInit, dataEnd, periodosReservados, id_usuario)
     // Verifica se todos os campos obrigatórios estão preenchidos
    if (!id_guia || !dataInit || !dataEnd || !periodosReservados || !id_usuario) {
        
    // Caso algum campo esteja faltando, retorna uma resposta de erro
      return res
        .status(400)
        .json({ erro: "Todos os campos devem ser preenchidos" });
    }

    // Verifica se dataInit é menor que dataEnd
    if (new Date(dataInit) >= new Date(dataEnd)) {
      return res.status(400).json({ erro: "A data de início deve ser menor que a data de fim" });
    }

    try {
      // Verifica se já existe uma reserva para o mesmo guia e no mesmo dia
      const overlapQuery = `SELECT * FROM reservas
                                WHERE id_guia = '${id_guia}' AND
                                (
                                  (dataInit <= '${dataInit}' AND dataEnd >= '${dataEnd}')
                                )`;

   // Consulta o banco de dados para verificar se há sobreposição de reservas
      connect.query(overlapQuery, function (err, results) {
    //retorna uma mensagem de erro no console
        if (err) {
          console.log(err);
          return res
            .status(500)
            .json({ error: "Erro ao verificar reserva existente" });
        }
 // Se já existir uma reserva, retorna um erro
        if (results.length > 0) {
          return res
            .status(400)
            .json({
              error: "Este guia não está disponível para esta data",
            });
        }

        // Calculando o número de dias entre dataInit e dataEnd
        const diffTime = Math.abs(new Date(dataEnd) - new Date(dataInit));
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        // Consultando o Valor do Período do guia
        const ValordoPeriodoQuery = `SELECT ValordoPeriodo FROM guia WHERE id_guia = '${id_guia}'`;

      // Consulta ao banco de dados para obter o valor do período reservado
        connect.query(ValordoPeriodoQuery, function (err, results) {
          if (err) {
            return res
              .status(500)
              .json({ error: "Erro ao consultar o valor do guia" });
          }
      // Se o guia não for encontrado, retorna um erro
          if (results.length === 0) {
            return res
              .status(404)
              .json({ error: "Guia não encontrado" });
          }

        // Extrai o valor do período da primeira linha de resultados
          const ValordoPeriodo = results[0].ValordoPeriodo;

          // Calcular o número total de períodos para cada dia da reserva
          const numPeriodosPorDia = periodosReservados.split(',').length;

          // Calcular o valor total
          const valorTotal = ValordoPeriodo * diffDays * numPeriodosPorDia;

          // Inserindo na tabela de reservas
          const insertQuery = `
          INSERT INTO reservas (id_guia, dataInit, dataEnd, periodosReservados, id_usuario, valorTotal)
          VALUES (
              '${id_guia}',
              '${dataInit}',
              '${dataEnd}',
              '${periodosReservados}',
              '${id_usuario}',
              ${valorTotal}
          )`;

        // Executa a inserção da reserva no banco de dados
          connect.query(insertQuery, function (err, results) {
            if (err) {
              console.error(err);
              return res
                .status(400)
                .json({ error: "Erro ao cadastrar reserva" });
            }

            // Enviando a resposta com o valorTotal calculado
            return res.status(201).json({
              message: "Reserva realizada com sucesso",
              valorTotal: valorTotal,
            });
          });
        });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }



  //Função para listar reservas
  //GET
  static async getAllReservas(req, res) {
    // Query para selecionar todas as reservas
    const query = `SELECT * FROM reservas`;

    connect.query(query, (err, result) => {
      if (err) {
    // Registra o erro no console e retorna uma resposta de erro 
        console.log("Erro: " + err);
        return res.status(500).json({ error: "Erro ao buscar reservas" });
      }
      if (result.length === 0) {
        return res.status(404).json({ message: "Reservas não encontradas" });
      }
      res
        .status(200)
        .json({ message: "Reservas encontradas", reservas: result });
    });
  }

  //Função para atualização da reserva
  //PUT
  static async updateReserva(req, res) {
  // Extrai o ID da reserva dos parâmetros da requisição
    const { id_reserva } = req.params;
   // Extrai os dados da reserva do corpo da requisição
    const { id_guia, dataInit, dataEnd, periodosReservados, id_usuario } = req.body;

    // Registro dos valores recebidos para depuração(correção)
    console.log(id_reserva, id_guia, dataInit, dataEnd, periodosReservados, id_usuario);

    if (!id_guia || !dataInit || !dataEnd || !periodosReservados || !id_usuario) {
    // Retorna uma resposta de erro caso algum campo esteja faltando
      return res
        .status(400)
        .json({ erro: "Todos os campos devem ser preenchidos" });
    }

    // Verificando se dataInit é menor que dataEnd
    if (new Date(dataInit) >= new Date(dataEnd)) {
      return res.status(400).json({ erro: "A data de início deve ser menor que a data de fim" });
    }

    try {
      // Verifica se já existe uma reserva para o mesmo guia e no mesmo dia
      const overlapQuery = `SELECT * FROM reservas
                                    WHERE id_guia = '${id_guia}' AND
                                    (
                                      (dataInit <= '${dataInit}' AND dataEnd >= '${dataEnd}')
                                    )`;

   // Consulta o banco de dados para verificar se há sobreposição de reservas
      connect.query(overlapQuery, function (err, results) {
    // Verifica se houve algum erro na execução da consulta
        if (err) {
          console.log(err);
          return res
            .status(500)
            .json({ error: "Erro ao verificar reserva existente" });
        }

        if (results.length > 0) {
          return res
            .status(400)
            .json({
              error: "Este guia não está disponível para esta data",
            });
        }

        // Calculando o número de dias entre dataInit e dataEnd
        const diffTime = Math.abs(new Date(dataEnd) - new Date(dataInit));
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        // Consultando o ValordoPeriodo do guia
        const ValordoPeriodoQuery = `SELECT ValordoPeriodo FROM guia WHERE id_guia = '${id_guia}'`;

        connect.query(ValordoPeriodoQuery, function (err, results) {
          if (err) {
            return res
              .status(500)
              .json({ error: "Erro ao consultar o valor do guia" });
          }

          if (results.length === 0) {
            return res
              .status(404)
              .json({ error: "guia não encontrado" });
          }

        // Extrai o valor do período da primeira linha de resultados
          const ValordoPeriodo = results[0].ValordoPeriodo;

          // Calcular o número total de períodos para cada dia da reserva
          const numPeriodosPorDia = periodosReservados.split(',').length;

          // Calcular o valor total
          const valorTotal = ValordoPeriodo * diffDays * numPeriodosPorDia;

        // Query para atualizar os dados da reserva no banco de dados
        const queryUpdate = `UPDATE reservas SET id_guia = ?, dataInit = ?, dataEnd = ?, periodosReservados = ?, id_usuario = ?, valorTotal = ? WHERE id_reserva = ?`;
        connect.query(queryUpdate, [id_guia, dataInit, dataEnd, periodosReservados, id_usuario, valorTotal, id_reserva], function (err, result) {
            if (err) {
            // Registra o erro no console e retorna uma resposta de erro
              console.error("Erro: " + err);
              return res.status(500).json({ error: "Erro ao atualizar sua reserva" });
            }
            // Verifica se houve alguma reserva afetada pela atualização
            if (result.affectedRows === 0) {
            // Retorna uma resposta indicando que a reserva não foi encontrada
              return res.status(404).json({ message: "Reserva não encontrada" });
            }
        // Retorna uma resposta de sucesso 
            res.status(200).json({ message: "Reserva atualizada com sucesso", "Valor Total": valorTotal });
          });
        });
      });
// Em caso de erro durante a execução do bloco try registra o erro no console e retorna uma resposta de erro 
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  //Função para deletar sua reserva
  //DELETE
  static async deleteReserva(req, res) {
// Extrai o ID da reserva dos parâmetros da requisição
    const { id_reserva } = req.params;
// Query para excluir a reserva com o ID especificado
    const query = `DELETE FROM reservas WHERE id_reserva = '${id_reserva}'`;

    // Executa a consulta de deletar no banco de dados
    connect.query(query, (err, result) => {
    // Verifica se houve algum erro na execução de deletar
      if (err) {
    // Registra o erro no console e retorna uma resposta de erro 
        console.log("Erro: " + err);
        return res.status(500).json({ error: "Erro ao excluir reserva" });
      }
    // Verifica se houve alguma reserva afetada pelo delete
      if (result.affectedRows === 0) {
        return res.status(404).json({ message: "Reserva não encontrada" });
      }
    // Retorna uma resposta de sucesso indicando que a reserva foi excluída com sucesso!
      res.status(200).json({ message: "Reserva excluída com sucesso" });
    });
  }
};
