const connect = require("../db/connect");

module.exports = class userController {

// Método para criar um novo guia
  static async postGuia(req, res) {

    try {
      const { nome, idiomas, valordoPeriodo, telefone, complemento, email } = req.body;
      // Inicializa a variável de mensagem de erro
      let msgerro = "";

      // Validações
      if (!email.includes("@")) {
    // Verifica se o email contém '@'
        msgerro = "O email deve conter '@'.";
      } else if (!/^[A-ZÀ-Ú]/.test(nome)) {
        msgerro = "O nome do guia deve começar com letra maiúscula.";
      } else if (
        nome === "" || 
        idiomas === "" || 
        valordoPeriodo === "" || 
        telefone === "" ||
        complemento === "" ||
        email === ""
      ) {
        msgerro = "Campos inválidos.";
      } else {
        msgerro = "Sucesso!";
      }

      console.log("Email", email);

      if (msgerro != "Sucesso!") {
        return res.status(400).json({ message: msgerro});
      } else {
        const query = `INSERT INTO guia (nome, idiomas, valordoPeriodo, telefone, complemento, email) VALUES (
          '${nome}', '${idiomas}', '${valordoPeriodo}', '${telefone}', '${complemento}', '${email}'
        )`;

        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res.status(500).json({ error: "Erro ao cadastrar guia" });
            return;
          }
          console.log("Inserido no Banco!!!");
          res.status(201).json({ message: "Guia cadastrado com sucesso." });
        });
      }

    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }


  // Editar o guia logado
  static async updateGuia(req, res) {
    const { id_guia } = req.params;
    const {nome, idiomas, valordoPeriodo, telefone, complemento, email } = req.body;
    let msgerro = "";

          // Validações
          if (!email.includes("@")) {
            msgerro = "O email deve conter '@'.";
          } else if (!/^[A-ZÀ-Ú]/.test(nome)) {
            msgerro = "O nome do guia deve começar com letra maiúscula.";
          } else if (
            nome === "" || 
            idiomas === "" || 
            valordoPeriodo === "" || 
            telefone === "" ||
            complemento === "" ||
            email === ""
          ) {
            msgerro = "Campos inválidos.";
    }

    if (msgerro) {
      return res.status(400).json({ error: msgerro });
    } else {
      const query = `UPDATE guia SET nome = ?, idiomas = ?, valordoPeriodo = ?, telefone = ?, complemento = ?, email = ? WHERE id_guia = ?`;
      const values = [nome, idiomas, valordoPeriodo, telefone, complemento, email, id_guia];
      
      connect.query(query, values, function (err) {
        if (err) {
          console.log(err);
          res.status(500).json({ error: "Erro ao atualizar o guia no banco!!!" });
        }
        console.log("Atualizado no Banco!!!");
        return res
          .status(200)
          .json({ message: "Guia atualizado com sucesso." });
      });
    }
  }


  //Deletar guia
  static async deleteGuia(req, res) {
    const { id } = req.body;

    const query = `DELETE FROM guia WHERE id_guia = ${id}`;

    connect.query(query, [id], function (err) {
      if (err) {
        if (err.code === 'ER_ROW_IS_REFERENCED_2' || err.errno === 1451) {
          // Código de erro específico para violação de chave estrangeira no MySQL
          return res.status(400).json({ error: "O guia não pode ser deletado pois está associado a uma reserva." });
        } else {
        console.log(err);
        res.status(500).json({ error: "Erro ao deletar o guia no banco!!!" });
      }
    }
      console.log("Deletado do Banco!!!");
    res.status(200).json({ message: "Guia deletado com sucesso."});
  });
}

   // Listar detalhes do guia
  static async getGuia(req, res) {
    const { id } = req.body;

    const query = `SELECT * FROM guia WHERE id_guia = ${id}`;
      connect.query(query, function (err, results) {
        if (err) {
          console.log(err);
          res.status(500).json({ error: "Erro ao buscar detalhes do guia " });
        }
        if (results.length === 0) {
          return res.status(404).json({ error: "Guia não encontrado" });
        }
        return res.status(200).json({ guia : results[0] });
      });
    }

};